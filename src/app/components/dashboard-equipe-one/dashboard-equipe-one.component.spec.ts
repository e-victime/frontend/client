import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DashboardEquipeOneComponent } from './dashboard-equipe-one.component';

describe('DashboardEquipeOneComponent', () => {
  let component: DashboardEquipeOneComponent;
  let fixture: ComponentFixture<DashboardEquipeOneComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DashboardEquipeOneComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DashboardEquipeOneComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
