import {Component, OnInit} from '@angular/core';
import {PersonnelService} from '../../services/personnel/personnel.service';
import {PosteService} from '../../services/poste/poste.service';
import {AuthService} from '../../services/auth/auth.service';
import {ActivatedRoute} from '@angular/router';

@Component({
    selector: 'app-dashboard-equipe-one',
    templateUrl: './dashboard-equipe-one.component.html',
    styleUrls: ['./dashboard-equipe-one.component.scss']
})
export class DashboardEquipeOneComponent implements OnInit {

    postes: Array<object> = [];
    equipe: any;
    idEquipe = this.router.snapshot.paramMap.get('id');


    constructor(private router: ActivatedRoute, private personnelService: PersonnelService, private posteService: PosteService, private auth: AuthService) {
    }

    ngOnInit(): void {

        this.posteService.getEquipeById(this.idEquipe)
            .subscribe(
                value => {
                    // @ts-ignore
                    this.equipe = value.content[0];
                    console.log(this.equipe);

                }
            );

    }

}
