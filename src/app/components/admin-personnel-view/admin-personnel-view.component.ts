import {Component, OnInit} from '@angular/core';
import {PersonnelService} from "../../services/personnel/personnel.service";
import {AuthService} from "../../services/auth/auth.service";

declare let $: any;

@Component({
    selector: 'app-admin-personnel-view',
    templateUrl: './admin-personnel-view.component.html',
    styleUrls: ['./admin-personnel-view.component.scss']
})
export class AdminPersonnelViewComponent implements OnInit {

    personnels: any;
    deleted: boolean;
    error: object;

    constructor(private personnelService: PersonnelService, private  auth: AuthService) {
    }

    ngOnInit(): void {

        $('.ui.vertical.segment').toggleClass('loading');

        this.personnelService.getAll()
            .subscribe(r => {
                    this.personnels = r.content;

                    if (this.personnels.length) {
                        $('.ui.vertical.segment').toggleClass('loading');
                        document.getElementById('no-content').remove();
                    } else {
                        $('.ui.vertical.segment').toggleClass('loading');
                        document.getElementById('content').remove();
                        document.getElementById('dummy-fill').remove();
                    }

                },
                error => {
                    this.error = error;
                    $('.ui.vertical.segment').toggleClass('loading');
                    document.getElementById('content').remove();
                    document.getElementById('dummy-fill').remove();
                });

    }

    deletePersonnnel(event, id: string) {

        let target = event.target || event.currentTarget || event.srcElement;
        target.classList.toggle('loading');

        if (confirm('Voulez-vous vraiment supprimer ce Personnel ? \nCes identifiants seront aussi supprimés de l\'authentification.')) {

            this.personnelService.delete(id)
                .subscribe(r => {
                        if (r === null) {
                            this.deleted = true;
                            this.personnelService.getAll()
                                .subscribe(r => {
                                        this.personnels = r.content;

                                        target.classList.toggle('loading');
                                    },
                                    error => {
                                        target.classList.toggle('loading');
                                    });
                        } else {
                            this.deleted = false;
                        }
                    },
                    error => {
                        target.classList.toggle('loading');
                    });

        } else {
            target.classList.toggle('loading');
        }

    }

    searchPersonnel(event) {

        $('.ui.vertical.segment').toggleClass('loading');

        if (event.target.value) {
            this.personnelService.getByUsername(event.target.value)
                .subscribe(r => {
                        this.personnels = r.content;
                        $('.ui.vertical.segment').toggleClass('loading');
                    },
                    error => {
                        this.error = error;
                        $('.ui.vertical.segment').toggleClass('loading');
                    });
        } else {
            this.personnelService.getAll()
                .subscribe(r => {
                        this.personnels = r.content;
                        $('.ui.vertical.segment').toggleClass('loading');
                    },
                    error => {
                        this.error = error;
                        $('.ui.vertical.segment').toggleClass('loading');
                    });
        }

    }

}
