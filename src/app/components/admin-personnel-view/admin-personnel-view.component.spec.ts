import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AdminPersonnelViewComponent } from './admin-personnel-view.component';

describe('AdminPersonnelViewComponent', () => {
  let component: AdminPersonnelViewComponent;
  let fixture: ComponentFixture<AdminPersonnelViewComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AdminPersonnelViewComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AdminPersonnelViewComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
