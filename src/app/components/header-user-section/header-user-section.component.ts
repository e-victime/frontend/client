import {Component, OnInit} from '@angular/core';
import {AuthService} from "../../services/auth/auth.service";

declare var $: any;

@Component({
    selector: 'header-user-section',
    templateUrl: './header-user-section.component.html',
    styleUrls: ['./header-user-section.component.scss']
})
export class HeaderUserSectionComponent implements OnInit {

    constructor(public auth: AuthService) {
        auth.afAuth.authState.subscribe(result => this.jqueryInit())
    }

    ngOnInit(): void {

    }

    jqueryInit() {
        $('#account').dropdown();
    }

}
