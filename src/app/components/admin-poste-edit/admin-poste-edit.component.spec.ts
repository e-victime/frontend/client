import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AdminPosteEditComponent } from './admin-poste-edit.component';

describe('AdminPosteEditComponent', () => {
  let component: AdminPosteEditComponent;
  let fixture: ComponentFixture<AdminPosteEditComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AdminPosteEditComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AdminPosteEditComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
