import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AdminEquipeViewComponent } from './admin-equipe-view.component';

describe('AdminEquipeViewComponent', () => {
  let component: AdminEquipeViewComponent;
  let fixture: ComponentFixture<AdminEquipeViewComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AdminEquipeViewComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AdminEquipeViewComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
