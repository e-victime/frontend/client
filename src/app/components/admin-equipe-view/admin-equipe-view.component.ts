import {Component, OnInit} from '@angular/core';
import {environment} from "../../../environments/environment";
import {PosteService} from "../../services/poste/poste.service";

declare let $: any;

@Component({
    selector: 'app-admin-equipe-view',
    templateUrl: './admin-equipe-view.component.html',
    styleUrls: ['./admin-equipe-view.component.scss']
})
export class AdminEquipeViewComponent implements OnInit {

    equipes: Array<object> = [];
    posteId: string;
    error: object;
    deleted: boolean;

    constructor(private posteService: PosteService) {
    }

    ngOnInit(): void {

        this.jqueryInit();

    }

    searchEquipes(id) {

        $('.ui.vertical.segment').toggleClass('loading');
        this.toggleDummyFill();

        this.posteService.getEquipesOfPoste(id)
            .subscribe(value => {

                    this.equipes = value.content;

                },
                error => {
                    this.error = error;
                    console.error("Error:", this.error);
                },
                () => {
                    $('.ui.vertical.segment').toggleClass('loading');
                    this.toggleDummyFill();
                });

    }

    toggleDummyFill() {

        if ($('#dummy-fill')) {
            $('#dummy-fill').remove();
        } else {
            $('.ui.vertical.segment').prepend(`

            <tr id="dummy-fill"></tr>

            `);
        }

    }

    jqueryInit() {

        $('#equipePosteView')
            .dropdown({
                onChange: (val) => {
                    this.searchEquipes(val);
                    this.posteId = val;
                },
                message: {
                    noResults: 'Pas de résultats trouvés...'
                },
                apiSettings: {
                    throttleFirstRequest: false,
                    throttle: 0,
                    minCharacters: 1,
                    url: environment.Api.Urls.poste + 'search/poste?search=( nom:{query}* OR nom:*{query}*)',
                    onResponse: (r) => {

                        let response = {
                            results: []
                        };

                        for (let poste of r.content) {

                            let result = {
                                name: "",
                                value: ""
                            };

                            result.name = poste.nom;
                            result.value = poste.id;

                            response.results.push(result);

                        }

                        return response;

                    }
                }
            });

    }

}
