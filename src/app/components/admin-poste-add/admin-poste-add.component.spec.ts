import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AdminPosteAddComponent } from './admin-poste-add.component';

describe('AdminPosteAddComponent', () => {
  let component: AdminPosteAddComponent;
  let fixture: ComponentFixture<AdminPosteAddComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AdminPosteAddComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AdminPosteAddComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
