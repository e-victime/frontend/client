import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DashboardResponsablePosteComponent } from './dashboard-responsable-poste.component';

describe('DashboardResponsablePosteComponent', () => {
  let component: DashboardResponsablePosteComponent;
  let fixture: ComponentFixture<DashboardResponsablePosteComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DashboardResponsablePosteComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DashboardResponsablePosteComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
