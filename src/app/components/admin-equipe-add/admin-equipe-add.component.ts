import {Component, OnInit} from '@angular/core';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {environment} from '../../../environments/environment';
import {PersonnelService} from '../../services/personnel/personnel.service';
import {PosteService} from '../../services/poste/poste.service';

declare let $: any;

@Component({
    selector: 'app-admin-equipe-add',
    templateUrl: './admin-equipe-add.component.html',
    styleUrls: ['./admin-equipe-add.component.scss']
})
export class AdminEquipeAddComponent implements OnInit {

    equipeForm: FormGroup;
    response: object;
    membres: Array<any> = [];
    membreTableHeader = false;
    chefEquipeIndex = 0;

    constructor(private fb: FormBuilder, private personnelService: PersonnelService, private posteService: PosteService) {
    }

    ngOnInit(): void {

        // JQUER INIT

        this.jqueryInit();

        // FORM

        this.equipeForm = this.fb.group(
            {
                poste: ['',
                    [
                        Validators.required
                    ]
                ],
                equipeName: ['',
                    [
                        Validators.required
                    ]
                ],
                idRadio: ['',
                    [
                        Validators.required
                    ]
                ],
                location: ['',
                    [
                        Validators.required
                    ]
                ],
                binome: ['',
                    []
                ],
                membres: ['',
                    [
                        Validators.required
                    ]
                ]
            }
        );

        this.membresEmptyStyle();

    }

    hasProp(o, name) {
        return o.hasOwnProperty(name);
    }

    get form() {
        return this.equipeForm.controls;
    }

    formEncodeEquipe() {
        return {
            nom: this.form.equipeName.value,
            binomeEquipe: !!this.form.binome.value,
            identifiantRadioEquipe: this.form.idRadio.value,
            localisation: this.form.location.value
        };
    }

    formEncodeMembre(membre) {

        return {
            idPersonnel: membre.id,
            role: membre.competence
        };

    }

    onSubmit() {

        if (this.equipeForm.valid) {

            document.getElementById('submit-form').classList.toggle('loading');

            this.posteService.addEquipe(this.form.poste.value, this.formEncodeEquipe())
                .subscribe(r => {
                        this.response = r;

                        if (this.membres.length) {

                            for (const membre of this.membres) {

                                this.posteService.addMembreToEquipe(this.form.poste.value, r.id, this.formEncodeMembre(membre))
                                    .subscribe(r => {
                                        },
                                        error => {
                                            console.error(error);
                                        });

                                if (membre.chefEquipe) {

                                    this.posteService.addChefEquipeToEquipe(this.form.poste.value, r.id, membre.id)
                                        .subscribe(value => {
                                            },
                                            error => {
                                                console.error(error);
                                            });

                                }

                            }
                        }

                    },
                    error => {
                        console.log(error);
                    },
                    () => {
                        document.getElementById('submit-form').classList.toggle('loading');
                    });

        }

    }

    membresEmptyStyle() {

        const addedMembres = $('#added-membres');

        if (this.membres.length) {

            if (!this.membreTableHeader) {
                this.addMembreTableHeader();
            }

            if (addedMembres.hasClass('membres-empty')) {
                addedMembres.toggleClass('membres-empty');
            }

        } else {

            if (this.membreTableHeader) {
                this.deleteMembreTableHeader();
            }

            if (!addedMembres.hasClass('membres-empty')) {
                addedMembres.toggleClass('membres-empty');
            }

        }

    }

    addMembreToForm(val) {


        if (this.form.membres.value === '') {
            this.form.membres.setValue('1');
        }

        if (!(this.form.binome.value && this.membres.length >= 2)) {
            if (!this.membres.length) {

                this.membres.push({
                    id: val.id,
                    nom: val.nom,
                    prenom: val.prenom,
                    username: val.username,
                    competence: 'PSE2',
                    chefEquipe: this.membres.length === 0
                });

            } else {

                let isIn = false;

                for (const membre of this.membres) {
                    // @ts-ignore
                    if (val.id === membre.id) {
                        isIn = !isIn;
                        break;
                    }
                }

                if (!isIn) {
                    this.membres.push({
                        id: val.id,
                        nom: val.nom,
                        prenom: val.prenom,
                        username: val.username,
                        competence: 'PSE2',
                        chefEquipe: false
                    });
                }

            }
        }

        this.membresEmptyStyle();

    }

    setMembreChefEquipe(event, idMembre) {

        this.membres[idMembre].chefEquipe = event.target.checked;

        this.chefEquipeIndex = idMembre;

        this.membres.forEach((element, index) => {
            if (idMembre !== index) {
                element.chefEquipe = false;
            }
        });

    }

    setMembreCompetence(event, idMembre) {

        this.membres[idMembre].competence = event.target.attributes.value.value;

    }

    setToBinome() {

        if (this.membres.length > 2) {
            this.membres = this.membres.slice(0, 2);
        }

        if (this.form.binome.value) {
            $('#binome-message').text('2 membres max.');
        } else {
            $('#binome-message').text('');
        }

    }

    addMembreTableHeader() {

        $('#added-membres').prepend(`

        <div class="ui top attached segment" id="membres-list-header">
            <div class="ui divided grid">
                <div class="row center aligned middle aligned">
                    <div class="four wide column">
                        <div class="ui small header">
                            Nom - Prénom - Nom d'utilisateur
                        </div>
                    </div>
                    <div class="four wide column">
                        <div class="ui small header">
                            Compétence
                        </div>
                    </div>
                    <div class="four wide column">
                        <div class="ui small header">
                            Chef de l'équipe
                        </div>
                    </div>
                    <div class="four wide column">
                        <div class="ui small header">
                            Action(s)
                        </div>
                    </div>
                </div>
            </div>
        </div>

        `);

        this.membreTableHeader = !this.membreTableHeader;

    }

    deleteMembreTableHeader() {
        $('#membres-list-header').remove();
        this.membreTableHeader = !this.membreTableHeader;
    }

    deleteMembre(index) {

        const wasChefEquipe = this.membres[index].chefEquipe;

        this.membres.splice(index, 1);

        if (wasChefEquipe && this.membres.length) {
            this.membres[0].chefEquipe = true;
            this.chefEquipeIndex = 0;
        }

        this.membresEmptyStyle();

        if (!this.membres.length) {
            if (this.form.membres.value) {
                this.form.membres.setValue('');
            }
        }

    }

    jqueryInit(): void {
        $('.ui.dropdown#equipePoste')
            .dropdown({
                onChange: (val) => {
                    this.form.poste.setValue(val);
                },
                message: {
                    noResults: 'Pas de résultats trouvés...'
                },
                apiSettings: {
                    throttleFirstRequest: false,
                    throttle: 0,
                    minCharacters: 1,
                    url: environment.Api.Urls.poste + 'search/poste?search=(nom:{query}* OR nom:*{query}*)',
                    onResponse: (r) => {

                        const response = {
                            results: []
                        };

                        for (const poste of r.content) {

                            const result = {
                                name: '',
                                value: ''
                            };

                            result.name = poste.nom;
                            result.value = poste.id;

                            response.results.push(result);

                        }

                        return response;

                    }
                }
            });

        $('.ui.dropdown#membresEquipe')
            .dropdown({
                onChange: (val) => {
                    if (val !== '') {

                        this.personnelService.get(val)
                            .subscribe(r => {

                                this.addMembreToForm(r);

                                $('.ui.dropdown#membresEquipe').dropdown('clear');

                            });

                    }
                },
                message: {
                    noResults: 'Pas de résultats trouvés...'
                },
                apiSettings: {
                    throttleFirstRequest: false,
                    throttle: 0,
                    minCharacters: 1,
                    url: environment.Api.Urls.personnel + 'search?search=(username:{query}* OR username:*{query}*)',
                    onResponse: (r) => {

                        const response = {
                            results: []
                        };

                        for (const personne of r.content) {

                            const result = {
                                name: '',
                                value: ''
                            };

                            result.name = `${personne.nom.toUpperCase()} ${personne.prenom} "${personne.username}"`;
                            result.value = personne.id;

                            response.results.push(result);

                        }

                        return response;

                    }
                }
            });

    }

    initNewDropdowns() {
        $('#added-membres .ui.selection.dropdown').dropdown('set selected', 'PSE2');
    }

}
