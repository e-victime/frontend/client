import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AdminEquipeAddComponent } from './admin-equipe-add.component';

describe('AdminEquipeAddComponent', () => {
  let component: AdminEquipeAddComponent;
  let fixture: ComponentFixture<AdminEquipeAddComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AdminEquipeAddComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AdminEquipeAddComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
