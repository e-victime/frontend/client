import {Component, OnInit} from '@angular/core';
import {FormBuilder, FormGroup, Validators} from "@angular/forms";
import {VictimeService} from "../../services/victime/victime.service";
import {ActivatedRoute} from "@angular/router";

declare let $: any;

@Component({
    selector: 'app-victime-declaration',
    templateUrl: './victime-declaration.component.html',
    styleUrls: ['./victime-declaration.component.scss']
})
export class VictimeDeclarationComponent implements OnInit {

    victimeForm: FormGroup;
    response: object;
    membres: Array<any> = [];
    membreTableHeader: boolean = false;
    chefEquipeIndex: number = 0;

    equipeId: string = this.router.snapshot.paramMap.get("idEquipe");
    posteId: string = this.router.snapshot.paramMap.get("idPoste");

    constructor(private fb: FormBuilder, private victimeService: VictimeService, private router: ActivatedRoute) {
    }

    ngOnInit(): void {

        // JQUERY INIT

        this.jqueryInit();

        // INIT FORM

        this.victimeForm = this.fb.group(
            {
                numeroVictime: ['',
                    [
                        Validators.required
                    ]
                ],
                firstname: ['',
                    [
                        Validators.required
                    ]
                ],
                lastname: ['',
                    [
                        Validators.required
                    ]
                ],
                gender: ['',
                    [
                        Validators.required,
                    ]
                ],
                birthdate: ['',
                    [
                        Validators.required,
                    ]
                ],
                adresse: ['',
                    [
                        Validators.required,
                    ]
                ],
                telephone: ['',
                    [
                        Validators.required,
                    ]
                ],
                personnePrevenir: ['',
                    [
                        Validators.required,
                    ]
                ],
                commentaire: ['',
                    [
                        Validators.required,
                    ]
                ],
            }
        );

    }

    formEncode() {

        return {
            numeroVictime: this.form.numeroVictime.value,
            dateNaissance: this.form.birthdate.value,
            sexe: this.form.gender.value,
            adresse: this.form.adresse.value,
            numero: this.form.telephone.value,
            personnePrevenir: this.form.personnePrevenir.value,
            commentaire: this.form.commentaire.value,
        };
    }

    onSubmit() {

        if (this.victimeForm.valid) {

            this.victimeService.addVictimeToEquipe(this.posteId, this.equipeId, this.formEncode())
                .subscribe(
                    value => {
                        console.log(value);
                    },
                    error => console.error(error)
                );

        }

    }

    get form() {
        return this.victimeForm.controls;
    }

    jqueryInit(): void {
        $('.ui.dropdown').dropdown();
    }

}
