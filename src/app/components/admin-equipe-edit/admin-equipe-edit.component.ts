import {Component, OnInit} from '@angular/core';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {CustomValidator} from '../../helpers/custom-validator';
import {ActivatedRoute} from '@angular/router';
import {environment} from '../../../environments/environment';
import {PersonnelService} from '../../services/personnel/personnel.service';
import {PosteService} from '../../services/poste/poste.service';

declare let $: any;

@Component({
    selector: 'app-admin-equipe-edit',
    templateUrl: './admin-equipe-edit.component.html',
    styleUrls: ['./admin-equipe-edit.component.scss']
})
export class AdminEquipeEditComponent implements OnInit {

    equipeForm: FormGroup;
    response: object;
    membresBefore: Array<number> = [];
    membres: Array<any> = [];

    membreTableHeader = false;
    chefEquipeIndex = 0;

    equipeId: string = this.router.snapshot.paramMap.get('idEquipe');
    posteId: string = this.router.snapshot.paramMap.get('idPoste');
    error: any;
    success: any;

    constructor(private fb: FormBuilder, private CV: CustomValidator, private router: ActivatedRoute, private personnelService: PersonnelService, private posteService: PosteService) {
    }

    async asyncForEach(array, callback) {
        for (let index = 0; index < array.length; index++) {
            await callback(array[index], index, array);
        }
    }

    ngOnInit(): void {

        // JQUER INIT

        this.jqueryInit();

        // FORM

        this.equipeForm = this.fb.group(
            {
                poste: ['',
                    [
                        Validators.required
                    ]
                ],
                equipeName: ['',
                    [
                        Validators.required
                    ]
                ],
                idRadio: ['',
                    [
                        Validators.required
                    ]
                ],
                location: ['',
                    [
                        Validators.required
                    ]
                ],
                binome: ['',
                    []
                ],
                membres: ['',
                    [
                        Validators.required
                    ]
                ]
            }
        );

        // FILL FORM AND VARS

        this.posteService.getEquipeOfPoste(this.posteId, this.equipeId)
            .subscribe(
                value => {


                    // Membres array fill

                    console.log(value);

                    this.form.membres.setValue('1');

                    this.asyncForEach(value.personnels, async (element, index) => {

                        const personnel = await this.personnelService.get(element.id.personnelId).toPromise()
                            .then(personnel => personnel);

                        this.membresBefore.push(personnel.id);

                        if (value.chefEquipe?.id && value.chefEquipe.id === personnel.id) {
                            this.chefEquipeIndex = index;
                        }

                        this.membres.push({
                            id: personnel.id,
                            nom: personnel.nom,
                            prenom: personnel.prenom,
                            username: personnel.username,
                            competence: element.role,
                            chefEquipe: value.chefEquipe?.id && (value.chefEquipe.id === element.id.personnelId)
                        });


                        this.membresEmptyStyle();

                    });

                    // Fill poste input

                    this.posteService.get(this.posteId)
                        .subscribe(poste => {

                            $('#equipePoste .default.text').text(poste.nom);
                            this.form.poste.setValue(poste.id);

                        });

                    // Fill rest of the form

                    this.form.equipeName.setValue(value.nom);
                    this.form.idRadio.setValue(value.identifiantRadioEquipe);
                    this.form.location.setValue(value.localisation);
                    this.form.binome.setValue(value.binomeEquipe);

                },
                error => {
                    this.error = error;
                },
                () => {
                    console.log(this.membres);
                });
        this.membresEmptyStyle();
    }

    hasProp(o, name) {
        return o.hasOwnProperty(name);
    }

    get form() {
        return this.equipeForm.controls;
    }

    formEncoded() {
        return {};
    }

    editPoste() {

        $('#equipePoste .ui.search.selection.dropdown').toggleClass('disabled');
        $('#equipePoste .default.text').text('Nom du poste');
        $('#equipePoste .ui.large.green.button').toggleClass('hidden');
        $('#equipePoste .ui.large.blue.button').toggleClass('hidden');

    }

    rollbackPoste() {

        $('#equipePoste .ui.search.selection.dropdown')
            .toggleClass('disabled')
            .dropdown('clear');

        this.posteService.get(this.posteId)
            .subscribe(poste => {
                $('#equipePoste .default.text')
                    .text(poste.nom);
                this.form.poste.setValue(poste.id);

            });

        $('#equipePoste .ui.large.blue.button').toggleClass('hidden');
        $('#equipePoste .ui.large.green.button').toggleClass('hidden');

    }

    membresEmptyStyle() {

        const addedMembres = $('#added-membres');

        if (this.membres.length) {

            if (!this.membreTableHeader) {
                this.addMembreTableHeader();
            }

            if (addedMembres.hasClass('membres-empty')) {
                addedMembres.toggleClass('membres-empty');
            }

        } else {

            if (this.membreTableHeader) {
                this.deleteMembreTableHeader();
            }

            if (!addedMembres.hasClass('membres-empty')) {
                addedMembres.toggleClass('membres-empty');
            }

        }

    }

    addMembreToForm(val) {


        if (this.form.membres.value === '') {
            this.form.membres.setValue('1');
        }

        if (!(this.form.binome.value && this.membres.length >= 2)) {
            if (!this.membres.length) {

                this.membres.push({
                    id: val.id,
                    nom: val.nom,
                    prenom: val.prenom,
                    username: val.username,
                    competence: 'PSE2',
                    chefEquipe: this.membres.length === 0
                });

            } else {

                let isIn = false;

                for (const membre of this.membres) {
                    // @ts-ignore
                    if (val.id === membre.id) {
                        isIn = !isIn;
                        break;
                    }
                }

                if (!isIn) {
                    this.membres.push({
                        id: val.id,
                        nom: val.nom,
                        prenom: val.prenom,
                        username: val.username,
                        competence: 'PSE2',
                        chefEquipe: false
                    });
                }

            }
        }

        this.membresEmptyStyle();

    }

    setMembreChefEquipe(event, idMembre) {

        this.membres[idMembre].chefEquipe = event.target.checked;

        this.chefEquipeIndex = idMembre;

        this.membres.forEach((element, index) => {
            if (idMembre !== index) {
                element.chefEquipe = false;
            }
        });

    }

    setMembreCompetence(event, idMembre) {

        this.membres[idMembre].competence = event.target.attributes.value.value;

    }

    setToBinome() {

        if (this.membres.length > 2) {
            this.membres = this.membres.slice(0, 2);
        }

        if (this.form.binome.value) {
            $('#binome-message').text('2 membres max.');
        } else {
            $('#binome-message').text('');
        }

    }

    addMembreTableHeader() {

        $('#added-membres').prepend(`

        <div class="ui top attached segment" id="membres-list-header">
            <div class="ui divided grid">
                <div class="row center aligned middle aligned">
                    <div class="four wide column">
                        <div class="ui small header">
                            Nom - Prénom - Nom d'utilisateur
                        </div>
                    </div>
                    <div class="four wide column">
                        <div class="ui small header">
                            Compétence
                        </div>
                    </div>
                    <div class="four wide column">
                        <div class="ui small header">
                            Chef de l'équipe
                        </div>
                    </div>
                    <div class="four wide column">
                        <div class="ui small header">
                            Action(s)
                        </div>
                    </div>
                </div>
            </div>
        </div>

        `);

        this.membreTableHeader = !this.membreTableHeader;

    }

    deleteMembreTableHeader() {
        $('#membres-list-header').remove();
        this.membreTableHeader = !this.membreTableHeader;
    }

    deleteMembre(index) {

        const wasChefEquipe = this.membres[index].chefEquipe;

        this.membres.splice(index, 1);

        if (wasChefEquipe && this.membres.length) {
            this.membres[0].chefEquipe = true;
            this.chefEquipeIndex = 0;
        }

        this.membresEmptyStyle();

        if (!this.membres.length) {
            if (this.form.membres.value) {
                this.form.membres.setValue('');
            }
        }

    }

    formEncodeEquipe() {

        return {
            nom: this.form.equipeName.value,
            binomeEquipe: !!this.form.binome.value,
            identifiantRadioEquipe: this.form.idRadio.value,
            localisation: this.form.location.value
        };
    }

    formEncodeMembre(membre) {

        return {
            idPersonnel: membre.id,
            role: membre.competence
        };

    }

    onSubmit() {

        if (this.equipeForm.valid) {

            document.getElementById('submit-form').classList.toggle('loading');

            this.posteService.editEquipe(this.posteId, this.equipeId, this.formEncodeEquipe())
                .subscribe(
                    r => {
                        this.response = r;
                    },
                    error => {
                    },
                    () => {

                        for (let i = 0; i < this.membres.length || i < this.membresBefore.length; i++) {

                            const membre = this.membres[i] ? this.membres[i] : null;
                            const membreBefore = this.membresBefore[i] ? this.membresBefore[i] : null;

                            if (membreBefore) {

                                if (membre) {

                                    if (membre.id !== membreBefore) {

                                        // IF THERE IS A DIFFERENCE BETWEEN LAST MEMBRES AND MEMBRES AFTER EDIT
                                        // DELETE OLD OCCURENCE
                                        this.posteService.deleteMembreFromEquipe(this.posteId, this.equipeId, `${membreBefore}`)
                                            .subscribe(
                                                value => {
                                                },
                                                error => {
                                                },
                                                () => {
                                                    // ADD NEW OCCURENCE
                                                    this.posteService.addMembreToEquipe(this.posteId, this.equipeId, this.formEncodeMembre(membre))
                                                        .subscribe(value => {
                                                        });
                                                });

                                    }

                                    if (membre.chefEquipe) {
                                        this.posteService.addChefEquipeToEquipe(this.posteId, this.equipeId, membre.id).subscribe(
                                            value => {
                                            },
                                            error => console.error(error),
                                        );
                                    }

                                } else {

                                    this.posteService.deleteMembreFromEquipe(this.posteId, this.equipeId, `${membreBefore}`)
                                        .subscribe(value => {
                                        });

                                }

                            } else {

                                if (membre) {

                                    this.posteService.addMembreToEquipe(this.posteId, this.equipeId, this.formEncodeMembre(membre))
                                        .subscribe(value => {
                                        });

                                }

                            }

                        }

                    });

        }

    }

    jqueryInit(): void {
        $('.ui.dropdown.equipePoste')
            .dropdown({
                onChange: (val) => {
                    this.form.poste.setValue(val);
                },
                message: {
                    noResults: 'Pas de résultats trouvés...'
                },
                apiSettings: {
                    throttleFirstRequest: false,
                    throttle: 0,
                    minCharacters: 1,
                    url: environment.Api.Urls.poste + 'search?search=(nom:{query}* OR nom:*{query}*)',
                    onResponse: (r) => {

                        const response = {
                            results: []
                        };

                        for (const poste of r.content) {

                            const result = {
                                name: '',
                                value: ''
                            };

                            result.name = poste.nom;
                            result.value = poste.id;

                            response.results.push(result);

                        }

                        return response;

                    }
                }
            });

        $('.ui.dropdown#membresEquipe')
            .dropdown({
                onChange: (val) => {
                    if (val !== '') {

                        this.personnelService.get(val)
                            .subscribe(r => {

                                this.addMembreToForm(r);

                                $('.ui.dropdown#membresEquipe').dropdown('clear');

                            });

                    }
                },
                message: {
                    noResults: 'Pas de résultats trouvés...'
                },
                apiSettings: {
                    throttleFirstRequest: false,
                    throttle: 0,
                    minCharacters: 1,
                    url: environment.Api.Urls.personnel + 'search?search=(username:{query}* OR username:*{query}*)',
                    onResponse: (r) => {

                        const response = {
                            results: []
                        };

                        for (const personne of r.content) {

                            const result = {
                                name: '',
                                value: ''
                            };

                            result.name = `${personne.nom.toUpperCase()} ${personne.prenom} "${personne.username}"`;
                            result.value = personne.id;

                            response.results.push(result);

                        }

                        return response;

                    }
                }
            });

    }

    initNewDropdowns(event, val) {
        $(event.nativeElement).dropdown('set selected', val);
    }

}
