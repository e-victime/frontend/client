import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AdminEquipeEditComponent } from './admin-equipe-edit.component';

describe('AdminEquipeEditComponent', () => {
  let component: AdminEquipeEditComponent;
  let fixture: ComponentFixture<AdminEquipeEditComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AdminEquipeEditComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AdminEquipeEditComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
