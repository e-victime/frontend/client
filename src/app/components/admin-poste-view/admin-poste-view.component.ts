import {Component, OnInit} from '@angular/core';
import {PosteService} from "../../services/poste/poste.service";

declare let $: any;

@Component({
    selector: 'app-admin-poste-view',
    templateUrl: './admin-poste-view.component.html',
    styleUrls: ['./admin-poste-view.component.scss']
})
export class AdminPosteViewComponent implements OnInit {

    postes: any;
    deleted: boolean;
    error: object;

    constructor(private posteService: PosteService) {
    }

    ngOnInit(): void {

        $('.ui.vertical.segment').toggleClass('loading');

        this.posteService.getAll()
            .subscribe(r => {
                    this.postes = r.content;

                    if (Object.keys(this.postes).length) {
                        $('.ui.vertical.segment').toggleClass('loading');
                        document.getElementById("no-content").remove();
                    } else {
                        $('.ui.vertical.segment').toggleClass('loading');
                        document.getElementById("content").remove();
                        document.getElementById("dummy-fill").remove();
                    }

                },
                error => {
                    this.error = error;
                    $('.ui.vertical.segment').toggleClass('loading');
                    document.getElementById('content').remove();
                    document.getElementById('dummy-fill').remove();
                });

    }

    deletePoste(event, id: number) {

        let target = event.target || event.currentTarget || event.srcElement;
        target.classList.toggle('loading');

        if (confirm('Voulez-vous vraiment supprimer ce Poste ?')) {

            this.posteService.delete(id)
                .subscribe(r => {
                    target.classList.toggle('loading');
                    if (r === null) {
                        this.deleted = true;
                        this.posteService.getAll()
                            .subscribe(r => {
                                this.postes = r.content;
                            });
                    } else {
                        this.deleted = false;
                    }
                });

        } else {
            target.classList.toggle('loading');
        }

    }

    searchPoste(event) {

        $('.ui.vertical.segment').toggleClass('loading');

        if (event.target.value) {
            this.posteService.getByName(event.target.value)
                .subscribe(r => {
                        this.postes = r.content;
                        $('.ui.vertical.segment').toggleClass('loading');
                    },
                    error => {
                        this.error = error;
                        $('.ui.vertical.segment').toggleClass('loading');
                    });
        } else {
            this.posteService.getAll()
                .subscribe(r => {
                        this.postes = r.content;
                        $('.ui.vertical.segment').toggleClass('loading');
                    },
                    error => {
                        this.error = error;
                        $('.ui.vertical.segment').toggleClass('loading');
                    });
        }
    }

}
