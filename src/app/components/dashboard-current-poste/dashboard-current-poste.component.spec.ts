import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DashboardCurrentPosteComponent } from './dashboard-current-poste.component';

describe('DashboardCurrentPosteComponent', () => {
  let component: DashboardCurrentPosteComponent;
  let fixture: ComponentFixture<DashboardCurrentPosteComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DashboardCurrentPosteComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DashboardCurrentPosteComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
