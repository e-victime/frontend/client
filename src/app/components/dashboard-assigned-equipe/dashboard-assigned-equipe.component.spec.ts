import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DashboardAssignedEquipeComponent } from './dashboard-assigned-equipe.component';

describe('DashboardAssignedEquipeComponent', () => {
  let component: DashboardAssignedEquipeComponent;
  let fixture: ComponentFixture<DashboardAssignedEquipeComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DashboardAssignedEquipeComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DashboardAssignedEquipeComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
