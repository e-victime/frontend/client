import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DashboardChefEquipeComponent } from './dashboard-chef-equipe.component';

describe('DashboardChefEquipeComponent', () => {
  let component: DashboardChefEquipeComponent;
  let fixture: ComponentFixture<DashboardChefEquipeComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DashboardChefEquipeComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DashboardChefEquipeComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
