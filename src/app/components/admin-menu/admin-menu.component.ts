import {Component, OnInit} from '@angular/core';

declare var $: any;

@Component({
    selector: 'admin-menu',
    templateUrl: './admin-menu.component.html',
    styleUrls: ['./admin-menu.component.scss']
})
export class AdminMenuComponent implements OnInit {

    sidebarOpen: boolean = false;
    screenWidth: number;

    ngOnInit(): void {

        this.screenWidth = window.innerWidth;

        window.addEventListener('resize', ev => {
            this.screenWidth = window.innerWidth;

            if (this.screenWidth > 768 && this.sidebarOpen) {
                this.sidebarToggle();
            }

        });

    }

    // tslint:disable-next-line:use-lifecycle-interface
    ngAfterViewInit(): void {
        this.jqueryInit();
    }

    jqueryInit() {
        $('#dashboard-admin-menu-personnel').dropdown();
        $('#dashboard-admin-menu-equipe').dropdown();
        $('#dashboard-admin-menu-centre').dropdown();
        $('#dashboard-admin-menu-poste').dropdown();
        $('#dashboard-admin-menu-role').dropdown();

    }

    sidebarToggle(): void {
        this.sidebarOpen = !this.sidebarOpen;
        $('.ui.sidebar.inverted.vertical.menu').sidebar('toggle');
    }

}
