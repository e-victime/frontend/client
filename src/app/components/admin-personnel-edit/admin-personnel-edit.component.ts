import {Component, OnInit} from '@angular/core';
import {ActivatedRoute} from '@angular/router';
import {PersonnelService} from '../../services/personnel/personnel.service';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {CustomValidator} from '../../helpers/custom-validator';

declare let $: any;

@Component({
    selector: 'app-admin-personnel-edit',
    templateUrl: './admin-personnel-edit.component.html',
    styleUrls: ['./admin-personnel-edit.component.scss']
})
export class AdminPersonnelEditComponent implements OnInit {

    personnelForm: FormGroup;
    response: object;

    personnelId: string = this.router.snapshot.paramMap.get('id');

    constructor(private router: ActivatedRoute, public personnelService: PersonnelService, private fb: FormBuilder, private CV: CustomValidator) {
    }

    ngOnInit(): void {

        // JQUERY INIT

        this.jqueryInit();

        // GET USER

        this.personnelForm = this.fb.group(
            {
                firstname: ['',
                    [
                        Validators.required
                    ]
                ],
                lastname: ['',
                    [
                        Validators.required
                    ]
                ],
                active: [''],
                phone: ['',
                    [
                        Validators.required,
                        Validators.minLength(9),
                        Validators.maxLength(9)
                    ]
                ],
                gender: ['',
                    [
                        Validators.required,
                    ]
                ],
                birthdate: ['',
                    [
                        Validators.required,
                    ]
                ],
                username: ['',
                    [
                        Validators.required,
                        Validators.minLength(5),
                        Validators.maxLength(30)
                    ]
                ],
                email: ['',
                    [
                        Validators.required,
                        Validators.email
                    ]
                ],
                password: ['',
                    [
                        Validators.minLength(5)
                    ]
                ],
                passwordConfirm: ['',
                    []
                ],
            },
            {
                validators: [
                    this.CV.passwordConfirm,
                    this.CV.usernameUsedEditValidator('username', this.router.snapshot.paramMap.get('id'))
                ]
            }
        );

        // FILL FORM WITH PERSONNEL VALUES

        this.personnelService.get(this.personnelId)
            .subscribe(r => {


                this.form.firstname.setValue(r.prenom);
                this.form.lastname.setValue(r.nom);
                this.form.active.setValue(r.actif ? 'checked' : '');
                this.form.gender.setValue(r.civilite);
                $('.ui.fluid.dropdown').dropdown({'set selected': r.civilite});
                this.form.birthdate.setValue(r.dateNaissance);
                this.form.username.setValue(r.username);
                this.form.email.setValue(r.email);
                this.form.password.setValue('');
                this.form.passwordConfirm.setValue('');
            });

    }

    get form() {
        return this.personnelForm.controls;
    }

    formEncoded() {
        const username: string = this.form.username.value;
        return {
            username: username.toLowerCase(),
            civilite: this.form.gender.value,
            prenom: this.form.firstname.value,
            nom: this.form.lastname.value,
            dateNaissance: this.form.birthdate.value,
            actif: this.form.active.value,
            email: this.form.email.value,
            password: this.form.password.value,
            phone: '+61' + this.form.phone.value
        };
    }

    onSubmit() {

        document.getElementById('submit-form').classList.toggle('loading');

        this.personnelService.edit(this.personnelId, this.formEncoded())
            .subscribe(r => {
                this.response = r;
                document.getElementById('submit-form').classList.toggle('loading');
            });


    }

    hasProp(o, name) {
        return o.hasOwnProperty(name);
    }

    jqueryInit(): void {
        $('.ui.dropdown').dropdown();
    }

}
