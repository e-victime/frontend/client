import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DashboardAssignedPosteComponent } from './dashboard-assigned-poste.component';

describe('DashboardAssignedPosteComponent', () => {
  let component: DashboardAssignedPosteComponent;
  let fixture: ComponentFixture<DashboardAssignedPosteComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DashboardAssignedPosteComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DashboardAssignedPosteComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
