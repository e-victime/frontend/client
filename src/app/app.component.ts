import {Component} from '@angular/core';
import {NotificationService} from './services/notification/notification.service';
import {AuthService} from './services/auth/auth.service';
import {PersonnelService} from './services/personnel/personnel.service';
import {PosteService} from './services/poste/poste.service';

@Component({
    selector: 'app-root',
    templateUrl: './app.component.html',
    styleUrls: ['./app.component.scss']
})
export class AppComponent {
    title = 'e-victime';
    private user: any;
    private postesId: Array<any> = [];

    constructor(private notificationService: NotificationService, private auth: AuthService, private personnelService: PersonnelService, private posteService: PosteService) {
    }

    async asyncForEach(array, callback) {
        for (let index = 0; index < array.length; index++) {
            await callback(array[index], index, array);
        }
    }

    // tslint:disable-next-line:use-lifecycle-interface
    ngOnInit() {

        this.personnelService.getByEmailStrict(this.auth.currentUser.email)
            .subscribe(
                value => {
                    this.user = value.content[0];

                    // @ts-ignore
                    this.posteService.getEquipesOfPersonnel(`${this.user.id}`)
                        .subscribe(
                            async value1 => {

                                await this.asyncForEach(value1.content, async (element) => {

                                    // @ts-ignore
                                    const equipes = await this.posteService.getEquipeById(element.id.equipeId).toPromise().then(equipe => equipe.content);

                                    for (const equipe of equipes) {
                                        this.postesId.push(equipe.posteId);
                                    }

                                });

                                await this.asyncForEach(this.postesId, async (element) => {

                                    const poste = await this.posteService.get(element).toPromise().then(value2 => value2);

                                    const dateDebut = new Date(poste.dateDebut).getTime();
                                    const dateFin = new Date(poste.dateFin).getTime();
                                    const dateMaintenant = new Date().getTime();

                                    if (dateDebut < dateMaintenant && dateMaintenant < dateFin) {
                                        console.log('oui');
                                        console.log(poste);
                                        this.notificationService.createNotification('Mission en cours !', 'Vous avez un poste en cours : ' + poste.nom);
                                    }

                                });

                            },
                            error => console.log(error)
                        );

                },
                error => console.log(error)
            );

    }

}
