import {AbstractControl, FormControl} from '@angular/forms';
import {PersonnelService} from '../services/personnel/personnel.service';
import {Injectable} from '@angular/core';

@Injectable({
    providedIn: 'root'
})
export class CustomValidator {

    constructor(private ps: PersonnelService) {
    }

    passwordConfirm = (control: AbstractControl) => {

        let errors = control.get('password').errors;

        if (control.get('password').value !== control.get('passwordConfirm').value) {
            if (errors) {
                errors.passwordConfirm = true;
                control.get('password').setErrors(errors);
            } else {
                control.get('password').setErrors({passwordConfirm: true});
            }
        } else {
            if (errors?.passwordConfirm) {
                delete errors.passwordConfirm;
                if (!Object.keys(errors).length) {
                    errors = null;
                }
                control.get('password').setErrors(errors);
            }
        }

    }

    usernameUsedValidator = (control: AbstractControl) => {

        let errors = control.errors;

        if (control.value !== '') {

            this.ps.getByUsernameStrict(control.value)
                .subscribe(r => {

                    if (r.content.length) {
                        if (errors) {
                            errors.usernameUsed = true;
                            control.setErrors(errors);
                        } else {
                            control.setErrors({usernameUsed: true});
                        }
                    } else {
                        if (errors?.usernameUsed) {
                            delete errors.usernameUsed;
                            if (!Object.keys(errors).length) {
                                errors = null;
                            }
                            control.setErrors(errors);
                        }
                    }

                });
        }

    }

    usernameUsedEditValidator = (controlName: string, id: string) => {

        return (control: AbstractControl) => {

            let errors = control.errors;

            if (control.get(controlName).value !== '') {

                this.ps.getByUsername(control.get(controlName).value)
                    .subscribe(r => {

                        console.log(r);

                        // tslint:disable-next-line:triple-equals
                        if (r.content.length && r.content[0].id != id) {
                            if (errors) {
                                errors.usernameUsed = true;
                                control.get(controlName).setErrors(errors);
                            } else {
                                control.get(controlName).setErrors({usernameUsed: true});
                            }
                        } else {
                            if (errors?.usernameUsed) {
                                delete errors.usernameUsed;
                                if (!Object.keys(errors).length) {
                                    errors = null;
                                }
                                control.get(controlName).setErrors(errors);
                            }
                        }

                    });
            }

        };

    }

    containSpace = (control: FormControl) => {

        let errors = control.errors;

        if (control.value !== '' && control?.value) {
            if (control.value.indexOf(' ') !== -1) {
                if (errors) {
                    errors.containSpace = true;
                    control.setErrors(errors);
                } else {
                    control.setErrors({containSpace: true});
                    console.log(control.errors);
                }
            } else {
                if (errors?.containSpace) {
                    delete errors.containSpace;
                    if (!Object.keys(errors).length) {
                        errors = null;
                    }
                    control.setErrors(errors);
                }
            }

        }

    }

}
