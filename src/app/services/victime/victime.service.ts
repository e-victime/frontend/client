import {Injectable} from '@angular/core';
import {HttpClient, HttpHeaders} from '@angular/common/http';
import {environment} from '../../../environments/environment';
import {Observable} from 'rxjs';

@Injectable({
    providedIn: 'root'
})
export class VictimeService {

    apiUrl: string = environment.Api.Urls.victime;

    httpOptions = {
        headers: new HttpHeaders({
            'Content-Type': 'application/json'
        })
    };

    constructor(private http: HttpClient) {
    }

    addVictimeToEquipe(idPoste: string, idEquipe: string, val: object): Observable<any> {
        return this.http.post(environment.Api.Urls.victime + idPoste + '/equipe/' + idEquipe + '/victime', JSON.stringify(val), this.httpOptions);
    }

}
