import {Injectable} from '@angular/core';

declare const Notification: any;

@Injectable({
    providedIn: 'root'
})
export class NotificationService {

    notification: typeof Notification;

    constructor() {
    }

    createNotification(title, body) {

        const notification = new Notification(title, {
            timeoutType: 'default',
            body
        });

        notification.addEventListener('click', () => {
            console.log('Notification clicked');
            notification.close();
        });

    }

}
