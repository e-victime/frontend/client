import {Routes} from "@angular/router";
import {LoginComponent} from "../components/login/login.component";
import {LoginGuard} from "./guards/login/login.guard";

export const AuthRoutes: Routes = [
    {
        path: '',
        component: LoginComponent,
        canActivate: [LoginGuard]
    }
];

