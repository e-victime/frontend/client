import {DashboardComponent} from '../components/dashboard/dashboard.component';
import {AuthGuard} from './guards/auth/auth.guard';
import {AdminGuard} from './guards/admin/admin.guard';
import {AdminComponent} from '../components/admin/admin.component';
import {Routes} from '@angular/router';
import {DashboardGuard} from './guards/dashboard/dashboard.guard';
import {AdminPersonnelAddComponent} from '../components/admin-personnel-add/admin-personnel-add.component';
import {AdminPersonnelViewComponent} from '../components/admin-personnel-view/admin-personnel-view.component';
import {AdminPersonnelEditComponent} from '../components/admin-personnel-edit/admin-personnel-edit.component';
import {AdminEquipeAddComponent} from '../components/admin-equipe-add/admin-equipe-add.component';
import {AdminEquipeEditComponent} from '../components/admin-equipe-edit/admin-equipe-edit.component';
import {AdminEquipeViewComponent} from '../components/admin-equipe-view/admin-equipe-view.component';
import {AdminPosteAddComponent} from '../components/admin-poste-add/admin-poste-add.component';
import {AdminPosteViewComponent} from '../components/admin-poste-view/admin-poste-view.component';
import {AdminPosteEditComponent} from '../components/admin-poste-edit/admin-poste-edit.component';
import {VictimeComponent} from '../components/victime/victime.component';
import {VictimeDeclarationComponent} from '../components/victime-declaration/victime-declaration.component';
import {DashboardAssignedPosteComponent} from '../components/dashboard-assigned-poste/dashboard-assigned-poste.component';
import {DashboardResponsablePosteComponent} from '../components/dashboard-responsable-poste/dashboard-responsable-poste.component';
import {DashboardEquipeOneComponent} from '../components/dashboard-equipe-one/dashboard-equipe-one.component';

export const UserRoutes: Routes = [
    {
        path: 'dashboard',
        component: DashboardComponent,
        canActivate: [AuthGuard, DashboardGuard],
        children: [
            {
                path: 'poste/assigned',
                component: DashboardAssignedPosteComponent
            },
            {
                path: 'poste/responsable',
                component: DashboardResponsablePosteComponent
            },
            {
                path: 'equipe/:id',
                component: DashboardEquipeOneComponent
            }
        ]
    },
    {
        path: 'victime',
        component: VictimeComponent,
        canActivate: [AuthGuard],
        children: [
            {
                path: 'declare/:idPoste/:idEquipe',
                component: VictimeDeclarationComponent
            }
        ]
    },
    {
        path: 'admin',
        component: AdminComponent,
        canActivate: [AuthGuard, AdminGuard],
        children: [
            {
                path: 'personnel/add',
                component: AdminPersonnelAddComponent
            },
            {
                path: 'personnel/view',
                component: AdminPersonnelViewComponent
            },
            {
                path: 'personnel/edit/:id',
                component: AdminPersonnelEditComponent
            },
            {
                path: 'equipe/add',
                component: AdminEquipeAddComponent
            },
            {
                path: 'equipe/view',
                component: AdminEquipeViewComponent
            },
            {
                path: 'equipe/edit/:idPoste/:idEquipe',
                component: AdminEquipeEditComponent
            },
            {
                path: 'poste/add',
                component: AdminPosteAddComponent
            },
            {
                path: 'poste/view',
                component: AdminPosteViewComponent
            },
            {
                path: 'poste/edit/:id',
                component: AdminPosteEditComponent
            }
        ]
    }
];
