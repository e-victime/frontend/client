# CHANGELOG

<!--- next entry here -->

## 1.4.1
2020-05-27

### Fixes

- **scripts:** Use yarn instead npm (777662d7cac00de83784c0602539877021386a52)

## 1.4.0
2020-05-26

### Features

- **personneladd/editcomponent:** Added phone number for firebase auth in api request + lint. (5c2bdc91ada21e271cfc06a23960d5bb0c8459bf)

### Fixes

- **customvalidators:** fixed username check when editing personnel. (56183a62d3ad153c2fead61c906902ea835d6207)
- **adminequipeaddcomponent:** fixed input search for personnels + lint. (b8677ef75caae4829f300e80f61d0639c42e1793)
- **posteadd/editcomponent:** fixed input search for personnels + lint. (d38d36a8b3ed2dd6bb339f21433c6f74de1de89c)

## 1.3.0
2020-05-25

### Features

- **notificationservice:** Add service for notifications. (0a6ac65d2fdb90953d0591574008740837f70f34)

### Fixes

- **npm:** add electron service package. (361d34113f38577bad70f17ab59beb4c1896a96a)
- **apis:** Updated API urls. (c16caae36112136cf1b4dcade7c8d277ba85362c)
- **electronmain:** Update electron global browser object variable. (085fde5e53fde392bbc1fc09306a7fba463480b3)
- **tslint:** Update some tslint options. (b38d7e07ba3ca71cd62d9b3150269c6ef75db287)

## 1.2.0
2020-05-25

### Features

- **npm:** add electron package (1735c2f0bf881578ffd342293bd5eb25bc386f57)

## 1.1.4
2020-05-21

### Fixes

- **dependencies:** Remove useless dependencies (9e979ce1bbf86e7265f4be8929cbfdb93a608a6d)
- **scripts:** change arguments in build commands (e9358b9cbdb96c80b35c65d17fb7ce8814d6f68a)

## 1.1.3
2020-05-21

### Fixes

- **ci:** Repair URLs in build_info (29ead8b42d52548ff7b9a33ff6eef95e3b78a2be)

## 1.1.2
2020-05-21

### Fixes

- **build:** Delete useless commands (24eb68a90f92aaa55170f7d45da0722556f1294b)

## 1.1.1
2020-05-21

### Fixes

- **ci:** Try fix Build in CI (87ea417f6f10f5e8c9172f6b44897f7ecd192c07)

## 1.1.0
2020-05-21

### Features

- **all:** Move from ionic to electron (dc1b480e5f9fb85e1dc83e78d4f19d3a8dce9d48)
- **build:** Add and configure electron build in package.json (977d00a98e882941dc980b311dbd3b9afb11609b)
- **ci:** Release packages (c84e3ea380c983497eb4fc768b66e988ea9c986b)
- **components:** enhanced responsiveness. (ef50f2987c956c42b584c63c8cb7b8de4074e3d2)

### Fixes

- **depencies:** Switch npm to yarn (13675280defd59c682dac3a891edc10da2a4dc91)
- **ignoring:** Add electrons build to gitignore (174ff5a3a05e2574872ef9ce8f174d84a746ee0e)
- **versioning:** Repair versioning in POM (487051d3d32f3f1ee07f94706748e14276a8f7c7)
- **ci:** Alpine image on version stage (dc55c6b9df4c240e3dae4a0560e582f0515ceffe)
- **dependencies:** Use local yarn version (b61ebcfc316217bd4ba0e233354bd403e7d42251)
- **environment:** changed api urls. (4c06c28fb290e1f49a47e8ff024f58746ee6c654)
- **gitignore:** add local script to gitignore. (cec8115ce866c90f0b1f68be947a2f1e28eba543)
- **ci:** Repair CI (97e97df31551e7232a85bdf329ce177ab53c7efc)
- **dependencies:** Change yarn version (7450f318a85ca67cd4e354d45d0d032e93eb35b3)
- **build:** Disable Windows package building (becea8c9bc70364f03f743f1e409c41a81b9c2c0)
- **build:** disable Windows build in package.json (ccfbbd07a175f8cbd6aa444b9bf9e762f08de072)
- **build:** Disable MacOS building and Enable Windows building (180b69f037ae358d5deb10868b21965feedd52b9)