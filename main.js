const {app, BrowserWindow} = require('electron');

let clientElecton;

function createWindow() {

    clientElecton = new BrowserWindow({
        width: 1280,
        height: 720,
    });

    clientElecton.loadURL(`file://${__dirname}/dist/app/index.html`);

    clientElecton.on('closed', function () {
        clientElecton = null;
    });

}

app.on('ready', createWindow);

app.on('window-all-closed', function () {
    if (process.platform !== 'darwin') {
        app.quit();
    }
});

app.on('activate', function () {
    if (clientElecton === null) {
        createWindow();
    }
})

